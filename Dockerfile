FROM peton3/php7.1-cli-test

WORKDIR /var/app
COPY . .
RUN composer install

ENTRYPOINT ["/var/app/calc"]
CMD ["0", "0"]
