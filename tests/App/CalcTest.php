<?php

namespace Tests\App;

use App\Calc;
use PHPUnit\Framework\TestCase;

class CalcTest extends TestCase
{
    public function testAdd()
    {
        $calc = new Calc();
        $this->assertEquals(3, $calc->add(1, 2));
    }
}
